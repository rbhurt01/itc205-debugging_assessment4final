package hotel.Testing;
import static org.junit.jupiter.api.Assertions.*;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import hotel.credit.CreditCard;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class TestingErrorInRecordService {
    Room mockRoom = new Room(201,RoomType.TWIN_SHARE);
    Guest guest = new Guest("Rohit bhurtel","Hurstville",123456789);
    List<ServiceCharge> list= new ArrayList<>();
    Booking booking;
    CreditCard card = new CreditCard(CreditCardType.VISA,2,2);
    SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
    double cost =300.00 ;

    @BeforeEach
    void setUp() throws Exception{
        dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        Date arrivalDate=dateFormat.parse("15-10-2018");
        int  stayLength=10;
        int occupantNumber=2;
        booking= new Booking(guest,mockRoom,arrivalDate,stayLength,occupantNumber,card);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    final void testAddServiceChargeFromBooking() throws Exception{
        list = booking.getCharges();
        booking.addServiceCharge(ServiceType.ROOM_SERVICE,cost);

        assertEquals(300.00,list.get(0).getCost());
    }

}
package hotel.Testing;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.text.SimpleDateFormat;
import java.util.Date;


import static org.junit.jupiter.api.Assertions.*;

class TestingBugInCheckOut {
    Room room = new Room(301,RoomType.TWIN_SHARE);
    Guest guest = new Guest("Rohit bhurtel","Hurstville",1234567);
    Booking booking;
    Hotel hotel = new Hotel();
    CreditCard card = new CreditCard(CreditCardType.VISA,2,2);
    SimpleDateFormat date=new SimpleDateFormat("dd-MM-yyyy");

    @BeforeEach
    void setUp() throws Exception{
        date=new SimpleDateFormat("dd-MM-yyyy");
        Date arrivalDate=date.parse("20-11-1996");
        int  stayLength=10;
        int occupantNumber=1;
        booking= new Booking(guest,room,arrivalDate,stayLength,occupantNumber,card);
    }

    @AfterEach
    void tearDown() throws Exception{
    }

    @Test
    final void testingBugInCheckOutMethod() throws Exception {
        hotel.activeBookingsByRoomId.put(booking.getRoomId(), booking);
        hotel.checkout(room.getId());
        assertEquals(0, hotel.activeBookingsByRoomId.size());
    }



}